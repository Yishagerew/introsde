package pojos;
import java.util.Calendar;
import java.util.Random;

public class Person {
	private String firstname;
	private String lastname;
	private long PersonId;
	private String birthdate;
	private HealthProfile hprofile;
	public Person(long PersonId,String firstname, String lastname,String birthdate,HealthProfile hprofile) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.hprofile = hprofile;
		this.PersonId=PersonId;
		this.birthdate=birthdate;
	}
	public Person(long PersonId,String firstname, String lastname, String birthdate) {
		super();
		this.PersonId=PersonId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.birthdate=birthdate;
		hprofile=new HealthProfile();
	}
	public Person() {
		//lazy initialization of variable personid
		Random personid = null;
		Random BirthDate=null;
		if(personid==null)
		{
			personid=new Random();
		}
		if(BirthDate==null)
		{
			BirthDate=new Random();
		}
		//getting current year using Calendar.getInstance
		int numberOfYears=1950+personid.nextInt(Calendar.getInstance().get(Calendar.YEAR)-1950);
		int numberOfDays=1+personid.nextInt(Calendar.DATE);
		int numberOfMonths=1+personid.nextInt(Calendar.MONTH);
firstname="yishagerew";
lastname="lulie";
this.hprofile=new HealthProfile();
//getting a random person Id between 1000 and 1
this.PersonId=personid.nextInt(1000)+1;
this.birthdate=numberOfYears+"-"+numberOfMonths+"-"+numberOfDays;
}
//Getter and setter methods for all class variables
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public HealthProfile getHprofile() {
		return hprofile;
	}
	public void setHprofile(HealthProfile hprofile) {
		this.hprofile = hprofile;
	}
	public long getPersonId() {
		return PersonId;
	}
	public void setPersonId(long personId) {
		PersonId = personId;
	}
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	
	
	
	
	

}
