import java.util.HashMap;
import java.util.Map;
import pojos.Person;
import pojos.HealthProfile;

public class HealthProfileReader {
	public static Map<Long,Person>database= new HashMap<>();
	static
	{
		Person pallino = new Person();
		Person pallo = new Person(12,"Pinco","Pallo","1990-2-3");
		HealthProfile hp = new HealthProfile(68.0,1.72);
		Person john = new Person(23,"John","Doe","1989-4-5",hp);
		database.put(pallino.getPersonId(), pallino);
		database.put(pallo.getPersonId(), pallo);
		database.put(john.getPersonId(), john);
	}
	public static void main(String[] args) {
		//initializeDatabase();
		int argCount = args.length;
		if(argCount == 0) {
			System.out.println("I deserve a person id.How coud I know without Id");
		} else if (argCount==1) {
			int PersonalId =Integer.parseInt(args[0]); 
			Person p= database.get(Long.valueOf(12)); 
			if (p!=null) { 
				System.out.println("A person with Id of"+PersonalId + p.getHprofile().toString());
			}
			else {	System.out.println(database.toString());
			System.out.println("Person with Id of "+PersonalId+ " is not in the database ");
			}
		} else if(argCount==2){
			Long PID=Long.parseLong(args[1]);
			displayHealthProfile(PID);
			}
	}
	public static void createPerson(Long PersonId,String firstName,String lastName,String birthDate)
	{
		Person newPerson=new Person(PersonId,firstName,lastName,birthDate);
		database.put(PersonId, newPerson);	
	}
	public static void displayHealthProfile(long PersonId)
	{
		if(database.containsKey(PersonId))
		{
			System.out.println("The contents of the personal information is");
			Person pObj=database.get(Long.valueOf(PersonId));
			System.out.println("Person Full Name is:"+pObj.getFirstname()+" "+ pObj.getLastname());
			System.out.println("Height and weight:"+pObj.getHprofile().toString());	
		}
		//Math.random(Math.floor(Math.random()*9998)+1);

	}
	public static void updateHealthProfile(long PersonId,Double height,Double weight)
	{
		Person pToUpdate=database.get(PersonId);
		HealthProfile hp2=new HealthProfile(height,weight);
		pToUpdate.setHprofile(hp2);
	}
}








