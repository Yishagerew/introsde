import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XPathTest {
	Document doc;
	XPath xpath;
	public void loaddocumentobjects() throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	    domFactory.setNamespaceAware(true);
	    DocumentBuilder builder = domFactory.newDocumentBuilder();
	    doc = builder.parse("books.xml");
	    loadxpathobjects();
	}
	public void loadxpathobjects()
	{
		 XPathFactory factory = XPathFactory.newInstance();
		 xpath = factory.newXPath();
	}
	public void ListOfBooksbyAuthors(String authorname) throws XPathExpressionException
	{
		System.out.println("/bookstore/book[author='"+authorname+"']");
		XPathExpression expr = xpath.compile("/bookstore/book[author='"+authorname+"']");
		System.out.println(doc.getChildNodes().getLength());
		Object result = expr.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;
	    for (int i = 0; i < nodes.getLength(); i++) {
	        System.out.println(nodes.item(i).getTextContent());
	    }
		
	}
	  public static void main(String[] args)  throws ParserConfigurationException, SAXException,
          											IOException, XPathExpressionException {
//		  String authorname="neal";
//		 String x= "/bookstore/book["+"author="+"'"+authorname+"'"+"]";
//		 System.out.println(x);

XPathTest demoobject=new XPathTest();
demoobject.loaddocumentobjects();
demoobject.ListOfBooksbyAuthors("George R. R. Martin");
	    

	  }
}
