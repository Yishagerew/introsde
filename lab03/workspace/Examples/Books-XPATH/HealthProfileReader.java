import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
public class HealthProfileReader {
	Document doc;
	XPath xpath;
	public void loaddocumentobjects() throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
	    domFactory.setNamespaceAware(true);
	    DocumentBuilder builder = domFactory.newDocumentBuilder();
	    doc = builder.parse("people.xml");
	    loadxpathobjects();
	}
	public void loadxpathobjects()
	{
		 XPathFactory factory = XPathFactory.newInstance();
		 xpath = factory.newXPath();
	}
	public void ListOfPeopleWithName(String firstname,String lastname) throws XPathExpressionException
	{
		System.out.println("/people/person[firstname='"+firstname+"' "+"and lastname='"+lastname+"']");
		
		XPathExpression expr = xpath.compile("/people/person[firstname='"+firstname+"' "+"and lastname='"+lastname+"']");
		System.out.println(doc.getChildNodes().getLength());
		Object result = expr.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;
	    for (int i = 0; i < nodes.getLength(); i++) {
	        System.out.println(nodes.item(i).getTextContent());
	    }
		
	}
	public void ListOfPeopleWithHeightCriteria(float height,String Op) throws XPathExpressionException
	{
		System.out.println("//ancestor::healthprofile[height"+Op+height+"]");
		//XPathExpression expr = xpath.compile("/people/person/healthprofile[height"+Op+height+"]");
		XPathExpression expr = xpath.compile("//firstname[healthprofile/height"+Op+height+"]");
		System.out.println(doc.getChildNodes().getLength());
		Object result = expr.evaluate(doc, XPathConstants.NODESET);
	    NodeList nodes = (NodeList) result;
	    for (int i = 0; i < nodes.getLength(); i++) {
	        System.out.println(nodes.item(i).getTextContent());
	    }
		
	}
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		
int options;
String Op;
float height;
HealthProfileReader personObject=new HealthProfileReader();
personObject.loaddocumentobjects();
Scanner scan=new Scanner(System.in);
System.out.println("Press 1 for finding people with their name");
System.out.println("Press 2 for finding people with their name");
options=scan.nextInt();
if(options==1)
{
System.out.println("Enter first name");
String firstname=scan.next();
System.out.println("Enter last name");
String lastname=scan.next();
String formattedfname=Character.toUpperCase(firstname.charAt(0))+firstname.substring(1).toLowerCase();
String formattedlname=Character.toUpperCase(lastname.charAt(0))+lastname.substring(1).toLowerCase();
personObject.ListOfPeopleWithName(formattedfname, formattedlname);
}
else if(options==2)
{
System.out.println("Enter the height of reference")	;
height=scan.nextFloat();
System.out.println("Enter Operator");
Op=scan.next();
personObject.ListOfPeopleWithHeightCriteria(height, Op);
}
else
System.out.println("you don know what you are doing");

	}

}
